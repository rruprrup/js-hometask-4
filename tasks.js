/**
 * 1. Напиши функцию createPromise, которая будет возвращать промис
 * */
function createPromise() {
  return new Promise(() => {
  });
}
/**
 * 2. Напиши функцию createResolvedPromise, которая будет возвращать промис, который успешно выполнен (fulfilled)
 * */

function createResolvedPromise() {
  return Promise.resolve();
}


/**
 * 3. Напиши функцию createResolvedPromiseWithData, которая будет возвращать промис, который успешно выполнен
 * (fulfilled) и возвращать при резолве объект вида  { success: true }
 * */

function createResolvedPromiseWithData() {
  let obj = { success: true };
  return Promise.resolve(obj);
}

/**
 * 4. Напиши функцию createRejectedPromise, которая будет возвращать промис, который будет отклонен (rejected)
 * */

function createRejectedPromise() {
  return new Promise((resolve, reject) => {
    reject();
  });
}

/**
 * 5. Напиши функцию createRejectedPromiseWithError, которая будет возвращать промис, который будет отклонен
 * (rejected) и отклоненыый промис должен возвращать объект ошибки с текстом "Что-то пошло не так"
 * */

//  Это задание — первое место, где встречаешь объект ошибки. Поэтому это словосочетание ничего не говорит.
//  И непонятно, почему задание решено неверно. (Пыталась вернуть объект obj: {Error: "Что-то пошло не так"}).
//  Было бы круто, если бы в тексте задания стояла  звёздочка с пояснением, что такое объект ошибки
//  или если бы в теории была бы ссылка. Загуглить самому — не сложно. Сложно понять, что это какой-то новый тип объекта.
function createRejectedPromiseWithError() {
  let obj = new Error("Что-то пошло не так");
  return Promise.reject(obj);
}

/**
 * 6. Напиши функцию fulfilledOrNotFulfilled, которая принимает на вход boolean аргумент и возвращает промис.
 * Если аргумент true, то промис должен быть выполнен, иначе — отклонен.
 * */

function fulfilledOrNotFulfilled(shouldBeResolve) {
  if (shouldBeResolve) {
    return Promise.resolve();
  } else {
    return Promise.reject();
  }
}

/**
 * 7. Создай функцию timer, которая возвращает промис, который выполняется через 3 секунды.
 * */

function timer() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, 3000);
  });
}

/**
 * 8. Используя async/await, напиши асинхронную функцию. Функция на вход принимает boolean аргумент и передает его в функцию из задания 6.
 * Если функция из задания 6 была зарезолвлена, возвращай 'success'.
 * Если зареджекчена, возвращай 'error'
 */

async function callOneAsyncFunction(shouldBeResolve) {
  try {
    await fulfilledOrNotFulfilled(shouldBeResolve);
    return "success"
  }
  catch (err) {
    return "error";
  }
}

/**
 * 9. Используя async/await, напиши асинхронную функцию. Функция должна получать результат из функции задания 3 и
 * передавать его в функцию из задания 6. Если функция из задания 6 была зарезолвлена, возвращай 'success'.
 */

async function callManyAsyncFunction() {
  try {
    let data = await createResolvedPromiseWithData();
    await fulfilledOrNotFulfilled(data.success);
    return "success";
  }
  catch (error) {
  }
}

module.exports = {
  createPromise,
  createResolvedPromise,
  createResolvedPromiseWithData,
  createRejectedPromise,
  createRejectedPromiseWithError,
  fulfilledOrNotFulfilled,
  timer,
  callOneAsyncFunction,
  callManyAsyncFunction
};
